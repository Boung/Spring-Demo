package com.boung.springdemo.repository;

import java.sql.SQLException;
import java.util.List;

import com.boung.springdemo.model.Student;

public interface StudentRepositoryInterface {

	List<Student> getAllStudents() throws SQLException;

	boolean createStudent(Student student) throws SQLException;
	
	boolean updateStudent(Student student) throws SQLException;

	
}
