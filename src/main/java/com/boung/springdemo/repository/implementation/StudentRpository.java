package com.boung.springdemo.repository.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.boung.springdemo.model.Student;
import com.boung.springdemo.repository.StudentRepositoryInterface;
import com.boung.springdemo.service.StudentServiceInterface;
import com.boung.springdemo.utilities.DateTimeOperation;

@Repository
public class StudentRpository implements StudentRepositoryInterface{

	@Autowired
	private DataSource datasource;
	
	
	public List<Student> getAllStudents() throws SQLException {
		String sql = "SELECT stu_id,stu_name,stu_gender,stu_dob,status  FROM tbl_student";
		List<Student> students = new ArrayList<>();
		
		try(Connection con = datasource.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.executeQuery();
			
			ResultSet rs = ps.getResultSet();
			
			Student stu = null;
			
					
			while (rs.next()) {
				stu = new Student();
				
				stu.setId(rs.getInt(1));
				stu.setName(rs.getString(2));
				stu.setGender(rs.getString(3));
				stu.setDob(rs.getString(4));
				stu.setStatus(rs.getString(5));
				
				students.add(stu);
			}
		}
		
		return students;
	}
	
	public boolean createStudent(Student student) throws SQLException {
		String sql = "INSERT INTO tbl_student (stu_name, stu_gender, stu_dob, status) VALUES(?,?,?,?)";
		try(Connection con = datasource.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, student.getName());
			ps.setString(2, student.getGender());
			ps.setTimestamp(3, new java.sql.Timestamp(DateTimeOperation.convertStringToDate(student.getDob(), "yyyy-MM-dd").getTime()));
			ps.setString(4, student.getStatus());
			
			return ps.executeUpdate() > 0 ;
		}
	}

	public boolean updateStudent(Student student) throws SQLException {
		String sql = "UPDATE tbl_student SET " + 
					"stu_name = ?, " + 
					"stu_gender = ?, " +
					"stu_dob = ?, " +
					"status = ?" +
					"WHERE stu_id = ?";
		try(Connection con = datasource.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, student.getName());
			ps.setString(2, student.getGender());
			ps.setTimestamp(3, new java.sql.Timestamp(DateTimeOperation.convertStringToDate(student.getDob(), "yyyy-MM-dd").getTime()));
			ps.setString(4, student.getStatus());
			ps.setInt(5, student.getId());
			
			return ps.executeUpdate() > 0 ;
		}
	}
}
