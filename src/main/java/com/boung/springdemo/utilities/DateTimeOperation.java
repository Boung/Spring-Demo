package com.boung.springdemo.utilities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeOperation {

	public static java.sql.Date convertStringToSqlDate(String date, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return new java.sql.Date(dateFormat.parse(date).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String convertDateToString(String format, Date sqlDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		if (sqlDate == null)
			return null;
		String date = dateFormat.format(sqlDate);
		return date;
	}

	public LocalDateTime convertStringToLocalDateTime(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy h:mm a");
		if (date == null || date.equals("")) {
			return null;
		}
		LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
		return localDateTime;
	}

	public LocalDateTime convertStringToLocalDateTime(String date, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		if (date == null || date.equals("")) {
			return null;
		}
		LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
		return localDateTime;
	}

	public static Date convertStringToDate(String date, String format) {
		DateFormat formatter = new SimpleDateFormat(format);
		if (date == null || date.equals("")) {
			return null;
		}
		Date formatedDate = null;
		try {
			formatedDate = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatedDate;
	}

	public String reverseLocalDateTimeToString(LocalDateTime date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy h:mm a");
		if (date == null) {
			return null;
		}
		String reverseDate = date.format(formatter);
		return reverseDate;
	}

	public String reverseLocalDateTimeToFormate(LocalDateTime date, String formate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formate);
		if (date == null) {
			return null;
		}
		String reverseDate = date.format(formatter);
		return reverseDate;
	}
}