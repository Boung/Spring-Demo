package com.boung.springdemo.controller;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.boung.springdemo.model.Student;
import com.boung.springdemo.service.implementation.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/get")
	public List<Student> getStudents() throws SQLException {
		
		return studentService.getAllStudents();
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public boolean createStudent(@RequestBody Student student) throws SQLException {		
		return studentService.createStudent(student);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public boolean updateStudent(@RequestParam("id") int id, @RequestBody Student student) throws SQLException {		
		student.setId(id);
		return studentService.updateStudent(student);
	}
}
