package com.boung.springdemo.service.implementation;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boung.springdemo.model.Student;
import com.boung.springdemo.repository.implementation.StudentRpository;
import com.boung.springdemo.service.StudentServiceInterface;

@Service
public class StudentService implements StudentServiceInterface {

	@Autowired
	private StudentRpository studentRepo;
	
	public List<Student> getAllStudents() throws SQLException {
		return studentRepo.getAllStudents();
	}

	public boolean createStudent(Student student) throws SQLException{
		return studentRepo.createStudent(student);
	}

	@Override
	public boolean updateStudent(Student student) throws SQLException {
		return studentRepo.updateStudent(student);
	}
	
	
}
